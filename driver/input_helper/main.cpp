//========= Copyright Valve Corporation ============//

#include <chrono>
#include <cstdlib>
#include <fstream>
#include <stdio.h>
#include <string>
#include <thread>

#include <openvr.h>

#include "../openvr/samples/shared/Matrices.h"
#include "../openvr/samples/shared/lodepng.h"
#include "../openvr/samples/shared/pathtools.h"

#if defined(POSIX)
#include "unistd.h"
#endif

#include "fifo.hpp"

#ifndef _countof
#define _countof(x) (sizeof(x) / sizeof((x)[0]))
#endif

void ThreadSleep(unsigned long nMilliseconds) {
#if defined(_WIN32)
  ::Sleep(nMilliseconds);
#elif defined(POSIX)
  usleep(nMilliseconds * 1000);
#endif
}

static bool g_bPrintf = true;

struct Action {
  Action(std::string path, std::string output_string)
      : output_string{output_string} {
    vr::VRInput()->GetActionHandle(path.c_str(), &handle);
  }

  vr::VRActionHandle_t handle;
  bool last_state;
  const std::string output_string;
};

//-----------------------------------------------------------------------------
// Purpose:
//------------------------------------------------------------------------------
class CMainApplication {
public:
  CMainApplication(int argc, char *argv[]);
  virtual ~CMainApplication();

  bool BInit();

  void Shutdown();

  void RunMainLoop();
  bool HandleInput();
  void ProcessVREvent(const vr::VREvent_t &event);
  void RenderFrame();

  Matrix4 GetHMDMatrixProjectionEye(vr::Hmd_Eye nEye);
  Matrix4 GetHMDMatrixPoseEye(vr::Hmd_Eye nEye);
  Matrix4 GetCurrentViewProjectionMatrix(vr::Hmd_Eye nEye);
  void UpdateHMDMatrixPose();

  Matrix4 ConvertSteamVRMatrixToMatrix4(const vr::HmdMatrix34_t &matPose);

private:
  bool m_bVerbose;
  bool m_bPerf;
  bool m_bVblank;
  bool m_bGlFinishHack;

  vr::IVRSystem *m_pHMD;
  std::string m_strDriver;
  std::string m_strDisplay;
  vr::TrackedDevicePose_t m_rTrackedDevicePose[vr::k_unMaxTrackedDeviceCount];
  Matrix4 m_rmat4DevicePose[vr::k_unMaxTrackedDeviceCount];

  std::ofstream fifo_stream;

  std::vector<Action> actions;

  vr::VRActionHandle_t right_hand_handle;

  vr::VRActionSetHandle_t m_actionsetDefault = vr::k_ulInvalidActionSetHandle;
};

//-----------------------------------------------------------------------------
// Purpose: Constructor
//-----------------------------------------------------------------------------
CMainApplication::CMainApplication(int argc, char *argv[])
    : m_pHMD(NULL), m_bVerbose(false), m_bPerf(false), m_bVblank(false),
      m_bGlFinishHack(true) {
  if (argc <= 1) {
    printf("Expected a fifo as the last argument");
    return;
  }

  fifo_stream = std::ofstream(argv[argc - 1], std::ios_base::out);
};

//-----------------------------------------------------------------------------
// Purpose: Destructor
//-----------------------------------------------------------------------------
CMainApplication::~CMainApplication() {
  // work is done in Shutdown
  printf("Shutdown");
}

//-----------------------------------------------------------------------------
// Purpose: Helper to get a string from a tracked device property and turn it
//			into a std::string
//-----------------------------------------------------------------------------
std::string GetTrackedDeviceString(vr::TrackedDeviceIndex_t unDevice,
                                   vr::TrackedDeviceProperty prop,
                                   vr::TrackedPropertyError *peError = NULL) {
  uint32_t unRequiredBufferLen = vr::VRSystem()->GetStringTrackedDeviceProperty(
      unDevice, prop, NULL, 0, peError);
  if (unRequiredBufferLen == 0)
    return "";

  char *pchBuffer = new char[unRequiredBufferLen];
  unRequiredBufferLen = vr::VRSystem()->GetStringTrackedDeviceProperty(
      unDevice, prop, pchBuffer, unRequiredBufferLen, peError);
  std::string sResult = pchBuffer;
  delete[] pchBuffer;
  return sResult;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
bool CMainApplication::BInit() {
  // Loading the SteamVR Runtime
  vr::EVRInitError eError = vr::VRInitError_None;
  m_pHMD = vr::VR_Init(&eError, vr::VRApplication_Background);

  if (eError != vr::VRInitError_None) {
    m_pHMD = NULL;
    printf("Unable to init VR runtime %s",
           vr::VR_GetVRInitErrorAsEnglishDescription(eError));
    return false;
  }

  int nWindowPosX = 700;
  int nWindowPosY = 100;

  m_strDriver = "No Driver";
  m_strDisplay = "No Display";

  m_strDriver = GetTrackedDeviceString(vr::k_unTrackedDeviceIndex_Hmd,
                                       vr::Prop_TrackingSystemName_String);
  m_strDisplay = GetTrackedDeviceString(vr::k_unTrackedDeviceIndex_Hmd,
                                        vr::Prop_SerialNumber_String);

  std::string strWindowTitle = "hellovr - " + m_strDriver + " " + m_strDisplay;

  auto manifest_path =
      Path_MakeAbsolute("../smolglove_daemon_actions.json",
                        Path_StripFilename(Path_GetExecutablePath()));
  printf("Manifest path: %s\n", manifest_path.c_str());
  // cube array
  vr::VRInput()->SetActionManifestPath(manifest_path.c_str());

  this->actions.push_back(
      Action("/actions/default/in/leftFingerClick", "left_click"));
  this->actions.push_back(
      Action("/actions/default/in/leftFingerTouch", "left_touch"));
  this->actions.push_back(
      Action("/actions/default/in/anyFingerRClick", "any_right_click"));
  this->actions.push_back(
      Action("/actions/default/in/rightFingerClick", "right_click"));
  this->actions.push_back(
      Action("/actions/default/in/rightFingerTouch", "right_touch"));

  vr::VRInput()->GetActionHandle("/actions/demo/in/righthand_anim",
                                 &this->right_hand_handle);

  vr::VRInput()->GetActionSetHandle("/actions/default", &m_actionsetDefault);

  return true;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CMainApplication::Shutdown() {
  if (m_pHMD) {
    vr::VR_Shutdown();
    m_pHMD = NULL;
  }
}

bool GetDigitalActionState(vr::VRActionHandle_t action,
                           vr::VRInputValueHandle_t *pDevicePath = nullptr) {
  vr::InputDigitalActionData_t actionData;
  vr::VRInput()->GetDigitalActionData(action, &actionData, sizeof(actionData),
                                      vr::k_ulInvalidInputValueHandle);
  if (pDevicePath) {
    *pDevicePath = vr::k_ulInvalidInputValueHandle;
    if (actionData.bActive) {
      vr::InputOriginInfo_t originInfo;
      if (vr::VRInputError_None ==
          vr::VRInput()->GetOriginTrackedDeviceInfo(
              actionData.activeOrigin, &originInfo, sizeof(originInfo))) {
        *pDevicePath = originInfo.devicePath;
      }
    }
  }
  return actionData.bActive && actionData.bState;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
bool CMainApplication::HandleInput() {
  bool bRet = false;

  // Process SteamVR events
  vr::VREvent_t event;
  while (m_pHMD->PollNextEvent(&event, sizeof(event))) {
    ProcessVREvent(event);
  }

  // Process SteamVR action state
  // UpdateActionState is called each frame to update the state of the actions
  // themselves. The application controls which action sets are active with the
  // provided array of VRActiveActionSet_t structs.
  vr::VRActiveActionSet_t actionSet = {0};
  actionSet.ulActionSet = m_actionsetDefault;
  vr::VRInput()->UpdateActionState(&actionSet, sizeof(actionSet), 1);

  vr::InputSkeletalActionData_t skeletal_data;
  vr::VRInput()->GetSkeletalActionData(this->right_hand_handle, &skeletal_data,
                                       sizeof(skeletal_data));


  uint32_t bone_count;
  vr::VRInput()->GetBoneCount(this->right_hand_handle, &bone_count);

  for (auto &&action : actions) {
    bool new_state = GetDigitalActionState(action.handle);

    if (new_state != action.last_state) {
      if (action.output_string == "any_right_click") {
        if (new_state) {
          ::system("xdotool mousedown 3");
        } else {
          ::system("xdotool mouseup 3");
        }
      }
      if (action.output_string == "left_click") {
        if (new_state) {
          ::system("xdotool mousedown 1");
        } else {
          ::system("xdotool mouseup 1");
        }
      }
      if (action.output_string == "right_click") {
        if (new_state) {
          ::system("xdotool mousedown 1");
        } else {
          ::system("xdotool mouseup 1");
        }
      } else {
        fifo_stream << action.output_string << "=" << new_state << std::endl;
        std::cout << action.output_string << "=" << new_state << std::endl;
      }
    }
    action.last_state = new_state;
  }

  // std::this_thread::sleep_for(std::chrono::milliseconds(10));

  return bRet;
}

//-----------------------------------------------------------------------------
// Purpose: Processes a single VR event
//-----------------------------------------------------------------------------
void CMainApplication::ProcessVREvent(const vr::VREvent_t &event) {
  switch (event.eventType) {
  case vr::VREvent_TrackedDeviceDeactivated: {
    printf("Device %u detached.\n", event.trackedDeviceIndex);
  } break;
  case vr::VREvent_TrackedDeviceUpdated: {
    printf("Device %u updated.\n", event.trackedDeviceIndex);
  } break;
  case vr::VREvent_ButtonPress: {
    printf("Buttonpress %i\n", event.trackedDeviceIndex);
  } break;
  }
}

void CMainApplication::RunMainLoop() {
  bool bQuit = false;

  while (!bQuit) {
    bQuit = HandleInput();
  }
}

//-----------------------------------------------------------------------------
// Purpose: Converts a SteamVR matrix to our local matrix class
//-----------------------------------------------------------------------------
Matrix4 CMainApplication::ConvertSteamVRMatrixToMatrix4(
    const vr::HmdMatrix34_t &matPose) {
  Matrix4 matrixObj(matPose.m[0][0], matPose.m[1][0], matPose.m[2][0], 0.0,
                    matPose.m[0][1], matPose.m[1][1], matPose.m[2][1], 0.0,
                    matPose.m[0][2], matPose.m[1][2], matPose.m[2][2], 0.0,
                    matPose.m[0][3], matPose.m[1][3], matPose.m[2][3], 1.0f);
  return matrixObj;
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
int main(int argc, char *argv[]) {
  printf("Starting smolglove daemon\n");

  CMainApplication *pMainApplication = new CMainApplication(argc, argv);

  if (!pMainApplication->BInit()) {
    pMainApplication->Shutdown();
    return 1;
  }

  pMainApplication->RunMainLoop();

  pMainApplication->Shutdown();

  return 0;
}
