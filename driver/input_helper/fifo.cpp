#include "fifo.hpp"

NamedFIFO::NamedFIFO(std::string const path)
	: path{path}
{
	mkfifo(path.c_str(), 0x666);
}
