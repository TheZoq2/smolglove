#pragma once

#include <string>

#ifdef __unix__
#include <sys/types.h>
#include <sys/stat.h>
#else
#error "A windows version is not currently implemented"
#endif


class NamedFIFO {
	public:
		NamedFIFO(std::string path);

	private:
		std::string path;
};
