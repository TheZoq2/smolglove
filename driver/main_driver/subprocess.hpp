#pragma once

#include <cstdlib>
#include <fstream>
#include <string>
#include <optional>

enum class SubprocessResult {
    Ok,
    FailedToStart,
    FailedToCreateFifo,
};

class Subprocess {
    public:
        // Create a new subprocess to be executed but does not start the subprocess
        // yet
        //
        // The second argument is the name of a fifo to use for communication
        // between the subprocesses. This will be passed as the last argument
        // to command. Output of the process is written to stdout
        Subprocess(std::string command, std::string fifo, std::string stdout);

        ~Subprocess();

        // Create the fifo and Start the subprocess in the background. If it is already running,
        // it is stopped and restarted using the  stop() function. Returns
        // false if the subprocess failed to start. Stdout of the subprocess is written
        // to 
        [[nodiscard]] SubprocessResult start();

        // Stops the subprocess and removes the associated fifo
        void stop();

        // Sets up the fifo but does not start the subprocess. Useful if the subprocess should
        // be run externally
        [[nodiscard]] SubprocessResult init();

        // Attempts to read a line from the associated fifo. Reads chars into
        // `buffer` and if there is a newline, returns the stored chars until
        // that and trims buffer to the remaining chars.
        std::optional<std::string> read_line();

    private:
        std::string command;
        std::string fifo;
        std::string stdout;

        std::optional<int> fifo_fd;

        std::optional<FILE*> stream;

        std::string buffer;
};
