//============ Copyright (c) Valve Corporation, All rights reserved.
//============

#include "driverlog.h"
#include "subprocess.hpp"
#include "util.hpp"
#include "poses.hpp"
#include <atomic>
#include <openvr_driver.h>

#include "../openvr/samples/shared/pathtools.h"

#include <chrono>
#include <optional>
#include <stdexcept>
#include <string>
#include <thread>
#include <vector>

#if defined(_WINDOWS)
#include <windows.h>
#endif

using namespace vr;

#if defined(_WIN32)
#define HMD_DLL_EXPORT extern "C" __declspec(dllexport)
#define HMD_DLL_IMPORT extern "C" __declspec(dllimport)
#elif defined(__GNUC__) || defined(COMPILER_GCC) || defined(__APPLE__)
#define HMD_DLL_EXPORT extern "C" __attribute__((visibility("default")))
#define HMD_DLL_IMPORT extern "C"
#else
#error "Unsupported Platform."
#endif

inline HmdQuaternion_t HmdQuaternion_Init(double w, double x, double y,
                                          double z) {
  HmdQuaternion_t quat;
  quat.w = w;
  quat.x = x;
  quat.y = y;
  quat.z = z;
  return quat;
}

inline void HmdMatrix_SetIdentity(HmdMatrix34_t *pMatrix) {
  pMatrix->m[0][0] = 1.f;
  pMatrix->m[0][1] = 0.f;
  pMatrix->m[0][2] = 0.f;
  pMatrix->m[0][3] = 0.f;
  pMatrix->m[1][0] = 0.f;
  pMatrix->m[1][1] = 1.f;
  pMatrix->m[1][2] = 0.f;
  pMatrix->m[1][3] = 0.f;
  pMatrix->m[2][0] = 0.f;
  pMatrix->m[2][1] = 0.f;
  pMatrix->m[2][2] = 1.f;
  pMatrix->m[2][3] = 0.f;
}

// keys for use with the settings API
static const char *const k_pch_Sample_Section = "driver_sample";
static const char *const k_pch_Sample_SerialNumber_String = "serialNumber";
static const char *const k_pch_Sample_ModelNumber_String = "modelNumber";
static const char *const k_pch_Sample_WindowX_Int32 = "windowX";
static const char *const k_pch_Sample_WindowY_Int32 = "windowY";
static const char *const k_pch_Sample_WindowWidth_Int32 = "windowWidth";
static const char *const k_pch_Sample_WindowHeight_Int32 = "windowHeight";
static const char *const k_pch_Sample_RenderWidth_Int32 = "renderWidth";
static const char *const k_pch_Sample_RenderHeight_Int32 = "renderHeight";
static const char *const k_pch_Sample_SecondsFromVsyncToPhotons_Float =
    "secondsFromVsyncToPhotons";
static const char *const k_pch_Sample_DisplayFrequency_Float =
    "displayFrequency";

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------

class CWatchdogDriver_Sample : public IVRWatchdogProvider {
public:
  CWatchdogDriver_Sample() { m_pWatchdogThread = nullptr; }

  virtual EVRInitError Init(vr::IVRDriverContext *pDriverContext);
  virtual void Cleanup();

private:
  std::thread *m_pWatchdogThread;
};

CWatchdogDriver_Sample g_watchdogDriverNull;

bool g_bExiting = false;

void WatchdogThreadFunction() {
  while (!g_bExiting) {
#if defined(_WINDOWS)
    // on windows send the event when the Y key is pressed.
    if ((0x01 & GetAsyncKeyState('Y')) != 0) {
      // Y key was pressed.
      vr::VRWatchdogHost()->WatchdogWakeUp(vr::TrackedDeviceClass_HMD);
    }
    std::this_thread::sleep_for(std::chrono::microseconds(500));
#else
    // for the other platforms, just send one every five seconds
    std::this_thread::sleep_for(std::chrono::seconds(5));
    vr::VRWatchdogHost()->WatchdogWakeUp(vr::TrackedDeviceClass_HMD);
#endif
  }
}

EVRInitError
CWatchdogDriver_Sample::Init(vr::IVRDriverContext *pDriverContext) {
  VR_INIT_WATCHDOG_DRIVER_CONTEXT(pDriverContext);
  InitDriverLog(vr::VRDriverLog());

  // Watchdog mode on Windows starts a thread that listens for the 'Y' key on
  // the keyboard to be pressed. A real driver should wait for a system button
  // event or something else from the the hardware that signals that the VR
  // system should start up.
  g_bExiting = false;
  m_pWatchdogThread = new std::thread(WatchdogThreadFunction);
  if (!m_pWatchdogThread) {
    DriverLog("Unable to create watchdog thread\n");
    return VRInitError_Driver_Failed;
  }

  return VRInitError_None;
}

void CWatchdogDriver_Sample::Cleanup() {
  g_bExiting = true;
  if (m_pWatchdogThread) {
    m_pWatchdogThread->join();
    delete m_pWatchdogThread;
    m_pWatchdogThread = nullptr;
  }

  CleanupDriverLog();
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
class CSampleControllerDriver : public vr::ITrackedDeviceServerDriver {
public:
  CSampleControllerDriver(std::string serial, std::string shadow_serial,
                          vr::ETrackedControllerRole hand)
      : subprocess{Path_StripFilename(Path_GetThisModulePath()) +
                       "/smolglove_daemon",
                   "/tmp/smolglove_fifo", "/tmp/smolglove_daemon_stdout"},
        m_hand{hand}, m_shadow_serial{shadow_serial} {
    m_unObjectId = vr::k_unTrackedDeviceIndexInvalid;
    m_ulPropertyContainer = vr::k_ulInvalidPropertyContainer;

    m_sSerialNumber = serial;

    m_sModelNumber = "MyController";
  }

  virtual ~CSampleControllerDriver() {}

  virtual EVRInitError Activate(vr::TrackedDeviceIndex_t unObjectId) {
    if (this->subprocess.init() != SubprocessResult::Ok) {
      DriverLog("Failed to init subprocess");
    } else {
      DriverLog("Started smolglove process");
    }
    DriverLog("Activating smolglove driver");

    m_unObjectId = unObjectId;
    m_ulPropertyContainer =
        vr::VRProperties()->TrackedDeviceToPropertyContainer(m_unObjectId);

    vr::VRProperties()->SetStringProperty(
        m_ulPropertyContainer, Prop_ModelNumber_String, m_sModelNumber.c_str());
    vr::VRProperties()->SetStringProperty(m_ulPropertyContainer,
                                          Prop_RenderModelName_String,
                                          m_sModelNumber.c_str());

    // return a constant that's not 0 (invalid) or 1 (reserved for Oculus)
    vr::VRProperties()->SetUint64Property(m_ulPropertyContainer,
                                          Prop_CurrentUniverseId_Uint64, 2);

    // avoid "not fullscreen" warnings from vrmonitor
    vr::VRProperties()->SetBoolProperty(m_ulPropertyContainer,
                                        Prop_IsOnDesktop_Bool, false);

    // our sample device isn't actually tracked, so set this property to avoid
    // having the icon blink in the status window
    // vr::VRProperties()->SetBoolProperty( m_ulPropertyContainer,
    // Prop_NeverTracked_Bool, true );

    // Set up some bones
    vr::VRBoneTransform_t* gripLimitTransforms = right_grip_limit_transforms;
    // untransform_bones(gripLimitTransforms, nBoneCount);

    // even though we won't ever track we want to pretend to be the right hand
    // so binding will work as expected
    vr::VRProperties()->SetInt32Property(m_ulPropertyContainer,
                                         Prop_ControllerRoleHint_Int32, m_hand);

    // this file tells the UI what to show the user for binding this controller
    // as well as what default bindings should be for legacy or other apps
    vr::VRProperties()->SetStringProperty(
        m_ulPropertyContainer, Prop_InputProfilePath_String,
        "{smolglove}/input/smolglove_profile.json");

    const char *pComponentName;
    const char *pSkeletonPath;

    if (is_left_hand()) {
      vr::VRProperties()->SetStringProperty(
          m_ulPropertyContainer, Prop_RenderModelName_String,
          "{indexcontroller}valve_controller_knu_1_0_left");

      pComponentName = "/input/skeleton/left";
      pSkeletonPath = "/skeleton/hand/left";
    } else {
      vr::VRProperties()->SetStringProperty(
          m_ulPropertyContainer, Prop_RenderModelName_String,
          "{indexcontroller}valve_controller_knu_1_0_right");

      pComponentName = "/input/skeleton/right";
      pSkeletonPath = "/skeleton/hand/right";
    }

    const char *pBasePosePath = "/pose/raw";

    vr::EVRInputError err = vr::VRDriverInput()->CreateSkeletonComponent(
        m_ulPropertyContainer, pComponentName, pSkeletonPath, pBasePosePath,
        vr::EVRSkeletalTrackingLevel::VRSkeletalTracking_Estimated,
        gripLimitTransforms, nBoneCount, &m_ulSkeletalComponentHandle);
    if (err != vr::VRInputError_None) {
      // Handle failure case
      DriverLog("CreateSkeletonComponent failed.  Error: %i\n", err);
    }
    DriverLog("Skeleton set up\n");

    // create all the input components
    vr::VRDriverInput()->CreateBooleanComponent(
        m_ulPropertyContainer, "/input/left_touch/click", &m_comp_left_touch);
    vr::VRDriverInput()->CreateBooleanComponent(
        m_ulPropertyContainer, "/input/left_click/click", &m_comp_left_click);
    vr::VRDriverInput()->CreateBooleanComponent(
        m_ulPropertyContainer, "/input/right_click/click", &m_comp_right_click);
    vr::VRDriverInput()->CreateBooleanComponent(
        m_ulPropertyContainer, "/input/right_touch/click", &m_comp_right_touch);

    // create our haptic component
    vr::VRDriverInput()->CreateHapticComponent(m_ulPropertyContainer,
                                               "/output/haptic", &m_compHaptic);

    return VRInitError_None;
  }

  bool is_left_hand() { return m_hand == vr::TrackedControllerRole_LeftHand; }

  virtual void Deactivate() {
    m_unObjectId = vr::k_unTrackedDeviceIndexInvalid;
    this->subprocess.stop();
  }

  virtual void EnterStandby() {}

  void *GetComponent(const char *pchComponentNameAndVersion) {
    // override this to add a component to a driver
    return NULL;
  }

  virtual void PowerOff() {}

  /** debug request from a client */
  virtual void DebugRequest(const char *pchRequest, char *pchResponseBuffer,
                            uint32_t unResponseBufferSize) {
    if (unResponseBufferSize >= 1)
      pchResponseBuffer[0] = 0;
  }

  DriverPose_t ComputePose() {
    vr::DriverPose_t newPose = {0};
    newPose.qWorldFromDriverRotation.w = 1;
    newPose.qDriverFromHeadRotation.w = 1;

    if (m_shadow_device.has_value()) {
      TrackedDeviceIndex_t shadow_pose = m_shadow_device.value();
      vr::TrackedDevicePose_t trackedDevicePoses[vr::k_unMaxTrackedDeviceCount];
      vr::VRServerDriverHost()->GetRawTrackedDevicePoses(
          0, trackedDevicePoses, vr::k_unMaxTrackedDeviceCount);

      const vr::TrackedDevicePose_t controllerPose =
          trackedDevicePoses[shadow_pose];
      if (controllerPose.bPoseIsValid) {
        // get the matrix that represents the position of the controller that we
        // are shadowing
        const vr::HmdMatrix34_t controllerMatrix =
            controllerPose.mDeviceToAbsoluteTracking;

        // get only the rotation (3x3 matrix), as the 3x4 matrix also includes
        // position
        const vr::HmdMatrix33_t controllerRotationMatrix =
            GetRotationMatrix(controllerMatrix);
        const vr::HmdQuaternion_t controllerRotation =
            GetRotation(controllerMatrix);

        // To get pose and rotation, open bot h
        vr::HmdVector3_t offset_vector;
        if (is_left_hand()) {
          offset_vector.v[0] = 0.07474; // - 0.07; // up/(-)down
          offset_vector.v[1] = -0.0852; // + 0.07; // (-)front/back
          offset_vector.v[2] = 0.04338;
        } else {
          offset_vector.v[0] = -0.0918; // + 0.07;
          offset_vector.v[1] = -0.0691; // + 0.07;
          offset_vector.v[2] = 0.0413;
        }
        const vr::HmdVector3_t vectorOffset =
            MultiplyMatrix(controllerRotationMatrix, offset_vector);

        // combine these positions to get the resultant position
        const vr::HmdVector3_t newControllerPosition =
            CombinePosition(controllerMatrix, vectorOffset);

        newPose.vecPosition[0] = newControllerPosition.v[0];
        newPose.vecPosition[1] = newControllerPosition.v[1];
        newPose.vecPosition[2] = newControllerPosition.v[2];

        newPose.qRotation = controllerRotation;

        vr::HmdQuaternion_t angle_offset;
        if (is_left_hand()) {
          angle_offset.w = 0.662;
          angle_offset.x = -0.270;
          angle_offset.y = -0.617;
          angle_offset.z = -0.329;
        } else {
          angle_offset.w = 0.687;
          angle_offset.x = -0.330;
          angle_offset.y = 0.603;
          angle_offset.z = 0.238;
        }

        newPose.qRotation =
            MultiplyQuaternion(controllerRotation, angle_offset);

        // Angular velocity
        // Converted from euler angle provided in world space to euler angle in
        // object space
        vr::HmdVector3_t angularVelocityWorld = controllerPose.vAngularVelocity;
        angularVelocityWorld.v[0] /= 100.0;
        angularVelocityWorld.v[1] /= 100.0;
        angularVelocityWorld.v[2] /= 100.0;

        vr::HmdQuaternion_t qAngularVelocityWorld =
            EulerToQuaternion(static_cast<double>(angularVelocityWorld.v[2]),
                              static_cast<double>(angularVelocityWorld.v[1]),
                              static_cast<double>(angularVelocityWorld.v[0]));

        vr::HmdQuaternion_t qAngularVelocityObject = MultiplyQuaternion(
            MultiplyQuaternion(QuatConjugate(newPose.qRotation),
                               qAngularVelocityWorld),
            newPose.qRotation);

        vr::HmdVector3_t angularVelocityObject =
            QuaternionToEuler(qAngularVelocityObject);

        newPose.vecAngularVelocity[0] =
            angularVelocityObject.v[0] * (double)100.0;
        newPose.vecAngularVelocity[1] =
            angularVelocityObject.v[1] * (double)100.0;
        newPose.vecAngularVelocity[2] =
            angularVelocityObject.v[2] * (double)100.0;

        newPose.vecVelocity[0] = controllerPose.vVelocity.v[0];
        newPose.vecVelocity[1] = controllerPose.vVelocity.v[1];
        newPose.vecVelocity[2] = controllerPose.vVelocity.v[2];

        newPose.poseIsValid = true;
        newPose.deviceIsConnected = true;

        newPose.result = vr::TrackingResult_Running_OK;

        // newPose.poseTimeOffset = poseConfiguration_.poseTimeOffset;
      } else {
        newPose.poseIsValid = false;
        newPose.deviceIsConnected = true;
        newPose.result = vr::TrackingResult_Uninitialized;
      }
    } else {
      newPose.result = vr::TrackingResult_Uninitialized;
      newPose.deviceIsConnected = false;
    }

    return newPose;
  }

  virtual DriverPose_t GetPose() { return this->ComputePose(); }

  void update_skeleton() {
    // vr::VRBoneTransform_t boneTransforms[NUM_BONES];
    // untransform_bones(boneTransforms, nBoneCount);
    vr::VRBoneTransform_t* boneTransforms = right_open_pose;
    if (is_left_hand())  {
      boneTransforms = left_open_pose;
      if (left_touch) {
        boneTransforms = left_closed_pose;
      }
    }
    else {
      if (right_touch) {
        boneTransforms = right_closed_pose;
      }
    }


    // Then update the WithController pose on the component with those
    // transforms
    vr::EVRInputError err = vr::VRDriverInput()->UpdateSkeletonComponent(
        m_ulSkeletalComponentHandle,
        vr::VRSkeletalMotionRange_WithController,
        boneTransforms, NUM_BONES
    );
    if (err != vr::VRInputError_None) {
      // Handle failure case
      DriverLog("UpdateSkeletonComponentfailed.  Error: %i\n", err);
    }

    // Then update the WithoutController pose on the component
    err = vr::VRDriverInput()->UpdateSkeletonComponent(
        m_ulSkeletalComponentHandle,
        vr::VRSkeletalMotionRange_WithoutController, boneTransforms,
        NUM_BONES);
    if (err != vr::VRInputError_None) {
      // Handle failure case
      DriverLog("UpdateSkeletonComponentfailed.  Error: %i\n", err);
    }
  }

  void RunFrame() {
#if defined(_WINDOWS)
    // Your driver would read whatever hardware state is associated with its
    // input components and pass that in to UpdateBooleanComponent. This could
    // happen in RunFrame or on a thread of your own that's reading USB state.
    // There's no need to update input state unless it changes, but it doesn't
    // do any harm to do so.

    vr::VRDriverInput()->UpdateBooleanComponent(
        m_compA, (0x8000 & GetAsyncKeyState('A')) != 0, 0);
    vr::VRDriverInput()->UpdateBooleanComponent(
        m_compB, (0x8000 & GetAsyncKeyState('B')) != 0, 0);
    vr::VRDriverInput()->UpdateBooleanComponent(
        m_compC, (0x8000 & GetAsyncKeyState('C')) != 0, 0);
#endif
    vr::DriverPose_t pose = this->ComputePose();
    vr::VRServerDriverHost()->TrackedDevicePoseUpdated(
        m_unObjectId, pose, sizeof(vr::DriverPose_t));

    // NOTE: This should probably be done in the serverdriver when we add more
    // controllers
    while (true) {
      auto str_opt = this->subprocess.read_line();
      if (!str_opt.has_value()) {
        break;
      }
      auto str = str_opt.value();

      auto eq_pos = str.find("=");
      if (eq_pos == std::string::npos) {
        DriverLog("Got string without = from daemon: %s\n", str.c_str());
      } else {
        auto key = str.substr(0, eq_pos);
        auto value = str.substr(eq_pos + 1);

        bool state;
        if (value == "0") {
          state = false;
        } else if (value == "1") {
          state = true;
        } else {
          // DriverLog("Unexpected value from fifo: %s\n", str.c_str());
          // continue;
        }

        if (key == "left_touch") {
          DriverLog("Setting left_touch to %i", state);
          EVRInputError error = vr::VRDriverInput()->UpdateBooleanComponent(
              m_comp_left_touch, 1, 0);
          DriverLog("Got left touch %i (error: %i)", state, error);
          left_touch = state;
        } else if (key == "left_click") {
          vr::VRDriverInput()->UpdateBooleanComponent(m_comp_left_click, 1, 0);
          DriverLog("Got left click %i", state);
        } else if (key == "right_touch") {
          DriverLog("Setting right_touch to %i", state);
          EVRInputError error = vr::VRDriverInput()->UpdateBooleanComponent(
              m_comp_right_touch, 1, 0);
          DriverLog("Set right touch %i (error: %i)", state, error);
          right_touch = state;
        } else if (key == "right_click") {
          vr::VRDriverInput()->UpdateBooleanComponent(m_comp_right_click, 1, 0);
          DriverLog("Got right click %i", state);
        } else if (key == "set_pose") {
          auto comma_pos = value.find(",");
          if(comma_pos == std::string::npos) {
            DriverLog("Got set_pose with a single parameter\n");
          }
          else {
            int idx;
            float new_val;
            auto idx_str = value.substr(0, comma_pos);
            auto float_str = value.substr(comma_pos + 1);
            DriverLog("set_pose [%s] %s\n", idx_str.c_str(), float_str.c_str());
            try {
              idx = std::stoi(idx_str);
              new_val = std::stof(float_str);
            } catch(std::invalid_argument const& e) {
              DriverLog("Invalid argument %s\n", e.what());
              continue;
            } catch(std::out_of_range const& e) {
              DriverLog("out of range %s\n", e.what());
              continue;
            }

            // DriverLog("Updating pose %i to %f\n", idx, new_val);

            int array_idx = idx / 8;
            int minor_idx = idx % 8;
            if (array_idx >= NUM_BONES) {
              DriverLog("OOB bone idx %i\n", array_idx);
            }
            DriverLog("[%i][%i] = %f\n", array_idx, minor_idx, new_val);
            switch (minor_idx) {
              case 0: editable_pose[array_idx].position.v[0] = new_val; break;
              case 1: editable_pose[array_idx].position.v[1] = new_val; break;
              case 2: editable_pose[array_idx].position.v[2] = new_val; break;
              case 3: editable_pose[array_idx].position.v[3] = new_val; break;
              case 4: editable_pose[array_idx].orientation.w = new_val; break;
              case 5: editable_pose[array_idx].orientation.x = new_val; break;
              case 6: editable_pose[array_idx].orientation.y = new_val; break;
              case 7: editable_pose[array_idx].orientation.z = new_val; break;
              default: DriverLog("Minor array overflow\n"); break;
            }
          }
        } else {
          DriverLog("Unrecognised string from fifo: %s", str.c_str());
        }
      }
    }

    update_skeleton();
  }

  void ProcessEvent(const vr::VREvent_t &vrEvent) {
    switch (vrEvent.eventType) {
    case vr::VREvent_TrackedDeviceUserInteractionStarted: {
      DriverLog("User interaction started");
    } break;
    case vr::VREvent_TrackedDeviceActivated: {
      DriverLog("New tracked device");

      auto device_index = vrEvent.trackedDeviceIndex;

      auto container =
          vr::VRProperties()->TrackedDeviceToPropertyContainer(device_index);

      auto model_number = vr::VRProperties()->GetStringProperty(
          container, Prop_TrackingSystemName_String);
      auto serial = vr::VRProperties()->GetStringProperty(
          container, Prop_SerialNumber_String);

      DriverLog("With serial %s", serial.c_str());
      if (serial == m_shadow_serial) {
        DriverLog("Found right tracker");
        m_shadow_device = device_index;
      }
    } break;
    case vr::VREvent_ButtonPress: {
      // if(m_shadow_device.has_value() && m_shadow_device.value() ==
      // vrEvent.trackedDeviceIndex) {
      DriverLog("Button press");
      // }
    }
    }
  }

  std::string GetSerialNumber() const { return m_sSerialNumber; }

  // We have 2 instances of this class, but only one will see the bool change in the
  // fifo, so we'll make a static shared atomic as a hack
  static std::atomic_bool left_touch;
  static std::atomic_bool right_touch;


private:
  vr::TrackedDeviceIndex_t m_unObjectId;
  vr::PropertyContainerHandle_t m_ulPropertyContainer;

  vr::VRInputComponentHandle_t m_comp_left_touch;
  vr::VRInputComponentHandle_t m_comp_left_click;
  vr::VRInputComponentHandle_t m_comp_right_touch;
  vr::VRInputComponentHandle_t m_comp_right_click;
  vr::VRInputComponentHandle_t m_compHaptic;

  vr::VRInputComponentHandle_t m_ulSkeletalComponentHandle;

  std::string m_sSerialNumber;
  std::string m_sModelNumber;

  Subprocess subprocess;

  std::optional<vr::TrackedDeviceIndex_t> m_shadow_device;

  vr::ETrackedControllerRole m_hand;
  std::string m_shadow_serial;

  
  // Number of skeleton bones
  const uint32_t nBoneCount = 31;
};

std::atomic_bool CSampleControllerDriver::left_touch = false;
std::atomic_bool CSampleControllerDriver::right_touch = false;

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
class CServerDriver_Sample : public IServerTrackedDeviceProvider {
public:
  virtual EVRInitError Init(vr::IVRDriverContext *pDriverContext);
  virtual void Cleanup();
  virtual const char *const *GetInterfaceVersions() {
    return vr::k_InterfaceVersions;
  }
  virtual void RunFrame();
  virtual bool ShouldBlockStandbyMode() { return false; }
  virtual void EnterStandby() {}
  virtual void LeaveStandby() {}

private:
  // CSampleDeviceDriver *m_pNullHmdLatest = nullptr;
  CSampleControllerDriver *m_leftController = nullptr;
  CSampleControllerDriver *m_rightController = nullptr;
};

CServerDriver_Sample g_serverDriverNull;

EVRInitError CServerDriver_Sample::Init(vr::IVRDriverContext *pDriverContext) {
  VR_INIT_SERVER_DRIVER_CONTEXT(pDriverContext);
  InitDriverLog(vr::VRDriverLog());

  // m_pNullHmdLatest = new CSampleDeviceDriver();
  // vr::VRServerDriverHost()->TrackedDeviceAdded(
  // m_pNullHmdLatest->GetSerialNumber().c_str(), vr::TrackedDeviceClass_HMD,
  // m_pNullHmdLatest );

  m_leftController = new CSampleControllerDriver(
      "smolglove_left", "LHR-DE0493B5", vr::TrackedControllerRole_LeftHand);
  vr::VRServerDriverHost()->TrackedDeviceAdded(
      m_leftController->GetSerialNumber().c_str(),
      vr::TrackedDeviceClass_Controller, m_leftController);
  m_rightController = new CSampleControllerDriver(
      "smolglove_right", "LHR-1EA099BC", vr::TrackedControllerRole_RightHand);
  vr::VRServerDriverHost()->TrackedDeviceAdded(
      m_rightController->GetSerialNumber().c_str(),
      vr::TrackedDeviceClass_Controller, m_rightController);

  return VRInitError_None;
}

void CServerDriver_Sample::Cleanup() {
  CleanupDriverLog();
  // delete m_pNullHmdLatest;
  // m_pNullHmdLatest = NULL;
  // NOTE: We should probably use unique_ptr here
  delete m_leftController;
  m_leftController = NULL;
  delete m_rightController;
  m_rightController = NULL;
}

void CServerDriver_Sample::RunFrame() {
  // if ( m_pNullHmdLatest )
  // {
  // 	m_pNullHmdLatest->RunFrame();
  // }
  if (m_leftController) {
    m_leftController->RunFrame();
  }
  if (m_rightController) {
    m_rightController->RunFrame();
  }

  vr::VREvent_t vrEvent;
  while (vr::VRServerDriverHost()->PollNextEvent(&vrEvent, sizeof(vrEvent))) {
    if (m_leftController) {
      m_leftController->ProcessEvent(vrEvent);
    }
    if (m_rightController) {
      m_rightController->ProcessEvent(vrEvent);
    }
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(1ms);
  }
}

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
HMD_DLL_EXPORT void *HmdDriverFactory(const char *pInterfaceName,
                                      int *pReturnCode) {
  if (0 == strcmp(IServerTrackedDeviceProvider_Version, pInterfaceName)) {
    return &g_serverDriverNull;
  }
  if (0 == strcmp(IVRWatchdogProvider_Version, pInterfaceName)) {
    return &g_watchdogDriverNull;
  }

  if (pReturnCode)
    *pReturnCode = VRInitError_Init_InterfaceNotFound;

  return NULL;
}
