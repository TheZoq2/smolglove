#include "subprocess.hpp"
#include "driverlog.h"
#include <cstdio>
#include <sys/stat.h>
#include <filesystem>
#include <stdio.h>
#include <fcntl.h>

Subprocess::Subprocess(std::string command, std::string fifo, std::string stdout)
    : command{command}
    , fifo{fifo}
    , stdout{stdout}
{}

Subprocess::~Subprocess() {
    this->stop();
}

SubprocessResult Subprocess::start() {
    auto init_result = this->init();
    if(init_result != SubprocessResult::Ok) {
        return init_result;
    }

    if(!std::filesystem::exists(command)) {
        DriverLog("Daemon %s does not exist", command.c_str());
        return SubprocessResult::FailedToStart;
    }
    auto full_command = (command + " " + fifo + " > " + stdout);


    DriverLog("Starting daemon %s", full_command.c_str());

    auto stream = popen(full_command.c_str(), "r");

    if (stream == nullptr) {
        DriverLog("Failed to launch subprocess");
        return SubprocessResult::FailedToStart;
    }

    this->stream = stream;

    return SubprocessResult::Ok;
}

SubprocessResult Subprocess::init() {
    if(this->stream.has_value()) {
        this->stop();
    }

    if(std::filesystem::exists(fifo)) {
        std::filesystem::remove(fifo);
    }

    if (mkfifo(fifo.c_str(), S_IRUSR | S_IWUSR)) {
        DriverLog("Failed to create fifo");
        return SubprocessResult::FailedToCreateFifo;
    }

    // Open the fifo for reading
    fifo_fd = ::open(fifo.c_str(), O_RDONLY | O_NONBLOCK);
    if(fifo_fd == -1) {
        DriverLog("Failed to open fifo_fd, errno: %i\n", errno);
    }
    // if(file == nullptr) {
    //     DriverLog("Failed top open fifo for reading");
    //     return SubprocessResult::FailedToCreateFifo;
    // }
    // fifo_fd = fileno(file);
    // // Set non-blocking
    // int flags = fcntl(fifo_fd, F_GETFL, 0);
    // fcntl(fifo_fd, F_SETFL, flags | O_NONBLOCK);

    // DriverLog("Opened fifo");


    return SubprocessResult::Ok;
}

void Subprocess::stop() {
    if(this->stream.has_value()) {
        auto stream = this->stream.value();

        pclose(stream);

        if (!std::filesystem::remove(fifo)) {
            DriverLog("Could not remove fifo, no such file");
        }
    }
}

std::optional<std::string> Subprocess::read_line() {
    if(fifo_fd.has_value()) {
        char buffer[256];

        auto count = read(fifo_fd.value(), buffer, 256);
        // No new data
        if(count == -1 && errno == EAGAIN) {
        }
        else if(count == -1) {
            DriverLog("Failed to read from fifo, errno: %s", strerror(errno));
        }
        else {
            this->buffer.append(buffer, count);
        }

        auto first_newline = this->buffer.find_first_of('\n');

        if(first_newline != std::string::npos) {
            std::string result = this->buffer.substr(0, first_newline);
            this->buffer = this->buffer.substr(first_newline + 1);
            return result;
        }
        return std::nullopt;
    }
    return std::nullopt;
}
