#pragma once
#include <openvr_driver.h>

const int NUM_BONES = 31;
extern vr::VRBoneTransform_t right_fist_pose[NUM_BONES];
extern vr::VRBoneTransform_t right_grip_limit_transforms[NUM_BONES];
extern vr::VRBoneTransform_t fully_closed_pose[NUM_BONES];
extern vr::VRBoneTransform_t editable_pose[NUM_BONES];


extern vr::VRBoneTransform_t right_open_pose[NUM_BONES];
extern vr::VRBoneTransform_t right_closed_pose[NUM_BONES];
extern vr::VRBoneTransform_t left_open_pose[NUM_BONES];
extern vr::VRBoneTransform_t left_closed_pose[NUM_BONES];
