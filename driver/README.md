# Smolglove driver

Simple OpenVR driver for the smolglove. Detects vive trackers (currently by
hard coded serial strings) and creates virtual controllers offset to match the
glove position.

To figure out the offsets for a custom glove, open the render model for both
the valve index knuckles, and vive tracker in blender. Remove any rotation on
the objects (they start with 90 degrees). Then rotate the index controller to
the correct pose relative to the tracker. Then move the tracker to where it
should be relative to the index controller.

After this, the offset vector for the driver is the negated position of the
tracker in blender, and the rotation is the quaternion value of the index
controller.

## Installation

Initialise submodules

```
git submodule update --init --recursive
```

Build the binaries

```
mkdir build
cd build
cmake ..
make
```

The binaries are placed in `bin/linux64` in the driver directory, but they need to be symlinked into the `bin/driver/bin/linux64` directory as shown below:

```
${PROJECT}/driver > ls bin/driver/bin/linux64 -la
lrwxrwxrwx 36 frans 27 Feb 12:15 driver_smolglove.so -> ../../../linux64/driver_smolglove.so
lrwxrwxrwx 33 frans 27 Feb 13:04 smolglove_daemon -> ../../../linux64/smolglove_daemon
```

Register the driver with `steamvr`

```
~/.local/share/Steam/steamapps/common/SteamVR/bin/vrpathreg.sh addriver $(pwd)/bin/driver
```


## Receiving input and the `input_helper`

In order to receive inputs, the `input_helper` is needed. It is a openvr
application which feeds actions to the driver via a named fifo. This is needed
as the driver itself has no way to receive inputs from the tracker as far as I
can tell.

## Binding for DCS (with OpenXR)

Binding is... weird, especially with OpenXR. The interaction profile of the smolglove driver in the steamvr configuration must be set to `oculus/oculus_touch` (top right corner)

Then, for the actual binding, you need to bind them on the left/right foot respectively. The thumb input is the touch. Click and right click are handled by the daemon and do not need to be bound. The input profile on the left/right foot controller should be none, not `oculus/oculus_touch`

To get hand interaction, the mapping should be for `right/left_controller_squeeze_v`

## Some other notes and failed attempts

Before writing this driver I attempted to re-orient the tracker when it was
acting as a controller and/or as a vive tracker held in hand. However, there
seems to be no way to define custom action poses for the first method, and
while you can change the rotation in the firmware of the device, changing the
position seems to have no effect.

