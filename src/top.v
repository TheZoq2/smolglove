// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
    input clk,

    input E2,
    output A1,
    output B1,
    output C5,
    output led
);
    // assign rst = pmod0[2];

    reg rst = 1;
    always @(posedge clk) begin
        rst <= 0;
    end

    wire sense_received;
    wire button_on;
    wire sense_out;

    assign A1 = sense_out;
    assign led = button_on;
    assign B1 = button_on;
    assign C5 = sense_received;


    e_main main
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_sense_unsync(E2)
        , .__output({sense_out, sense_received, button_on})
        );
endmodule

